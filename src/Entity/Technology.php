<?php

namespace App\Entity;

use App\Repository\TechnologyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=TechnologyRepository::class)
 * @Vich\Uploadable
 */
class Technology
{
   /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
   private ?int $id;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Length(
    *    min=3, minMessage="Le nom de la technomogie est trop courte, {{ limit }} caractères minimum",
    *    max=80, maxMessage="Le nom de la technomogie est trop longue, {{ limit }} caractères maximum"
    * )
    */
   public ?string $name;

   /**
    * @ORM\OneToOne(targetEntity=Images::class)
    * @Assert\NotBlank(
    *    message="Vous devez selectionner une image"
    * )
    */
   private ?Images $image;

   /**
    * @ORM\ManyToMany(targetEntity=Project::class, mappedBy="technology")
    */
   private $projects;

   /**
    * @ORM\ManyToOne(targetEntity=Categories::class, inversedBy="technologies")
    */
   private $category;

   public function __construct()
   {
      $this->projects = new ArrayCollection();
   }

   public function getId(): ?int
   {
      return $this->id;
   }

   public function getName(): ?string
   {
      return $this->name;
   }

   public function setName(string $name): self
   {
      $this->name = $name;

      return $this;
   }

   public function getImage(): ?Images
   {
      return $this->image;
   }

   public function setImage(?Images $image): self
   {
      $this->image = $image;

      return $this;
   }

   /**
    * @return Collection|Project[]
    */
   public function getProjects(): Collection
   {
      return $this->projects;
   }

   public function addProject(Project $project): self
   {
      if (!$this->projects->contains($project)) {
         $this->projects[] = $project;
         $project->addTechnology($this);
      }

      return $this;
   }

   public function removeProject(Project $project): self
   {
      if ($this->projects->removeElement($project)) {
         $project->removeTechnology($this);
      }

      return $this;
   }

   public function getCategory(): ?Categories
   {
       return $this->category;
   }

   public function setCategory(?Categories $category): self
   {
       $this->category = $category;

       return $this;
   }
}
