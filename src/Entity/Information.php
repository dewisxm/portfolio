<?php

namespace App\Entity;

use App\Repository\InformationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InformationRepository::class)
 */
class Information
{
   /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
   private $id;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Length(
    *    min=3, minMessage="Votre nom est trop court, {{ limit }} caractères minimum",
    *    max=80, maxMessage="Votre nom est trop long, {{ limit }} caractères maximum"
    * )
    */
   private $lastname;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Length(
    *    min=3, minMessage="Votre prénom est trop court, {{ limit }} caractères minimum",
    *    max=80, maxMessage="Votre prénom est trop long, {{ limit }} caractères maximum"
    * )
    */
   private $firstname;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank(
    *    message="Ce champs est requis"
    * )
    * @Assert\Email(
    *    message="Votre email n'est pas valide"
    * )
    */
   private $email;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank(
    *    message="Ce champs est requis"
    * )
    * @Assert\Length(
    *    max=80, maxMessage="Votre job est trop long, {{ limit }} caractères maximum"
    * )
    */
   private $job;

   /**
    * @ORM\Column(type="string", nullable=true)
    * @Assert\Regex(
    *    pattern="/^0[0-9]{1}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}$/",
    *    message="Votre numéro de téléphone n'est pas valide"
    * )
    */
   private $phone;

   /**
    * @ORM\OneToOne(targetEntity=Images::class, cascade={"persist", "remove"})
    * @Assert\NotBlank()
    */
   private $image;

   /**
    * @ORM\Column(type="text")
    */
   private $description;

   /**
    * @ORM\Column(type="string", length=255)
    */
   private $city;

   /**
    * @ORM\Column(type="string", length=255)
    */
   private $postal_code;

   public function getId(): ?int
   {
      return $this->id;
   }

   public function getLastname(): ?string
   {
      return $this->lastname;
   }

   public function setLastname(string $lastname): self
   {
      $this->lastname = $lastname;

      return $this;
   }

   public function getFirstname(): ?string
   {
      return $this->firstname;
   }

   public function setFirstname(string $firstname): self
   {
      $this->firstname = $firstname;

      return $this;
   }

   public function getEmail(): ?string
   {
      return $this->email;
   }

   public function setEmail(string $email): self
   {
      $this->email = $email;

      return $this;
   }

   public function getJob(): ?string
   {
      return $this->job;
   }

   public function setJob(string $job): self
   {
      $this->job = $job;

      return $this;
   }

   public function getPhone(): ?string
   {
      return $this->phone;
   }

   public function setPhone(?string $phone): self
   {
      $this->phone = $phone;

      return $this;
   }

   public function getImage(): ?Images
   {
      return $this->image;
   }

   public function setImage(?Images $image): self
   {
      $this->image = $image;

      return $this;
   }

   public function getDescription(): ?string
   {
      return $this->description;
   }

   public function setDescription(string $description): self
   {
      $this->description = $description;

      return $this;
   }

   public function getCity(): ?string
   {
       return $this->city;
   }

   public function setCity(string $city): self
   {
       $this->city = $city;

       return $this;
   }

   public function getPostalCode(): ?string
   {
       return $this->postal_code;
   }

   public function setPostalCode(string $postal_code): self
   {
       $this->postal_code = $postal_code;

       return $this;
   }
}
