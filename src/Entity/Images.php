<?php

namespace App\Entity;

use App\Repository\ImagesRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ImagesRepository::class)
 * @Vich\Uploadable
 */
class Images
{
   /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
   private ?int $id;

   /**
    * @ORM\Column(type="string", length=255, nullable=true)
    * @var string|null
    */
   private ?string $image = null;

   /**
    * @Vich\UploadableField(mapping="products", fileNameProperty="image")
    * @var File|null
    * @Assert\NotBlank(
    *    message="Vous devez ajouter une image"
    * )
    */
   private ?File $imageFile = null;

   /**
    * @ORM\Column(type="datetime")
    * @var DateTime
    */
   private DateTime $updatedAt;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank()
    * @Assert\Length(
    *    max=80,
    *    maxMessage="Le nom de l'image est trop long"
    * )
    */
   private ?string $name;

   /**
    * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="picture")
    */
   private ?Project $project;

   /**
    * @ORM\OneToOne(targetEntity=Social::class, mappedBy="image", cascade={"persist", "remove"})
    */
   private ?Social $social;

   /**
    * @ORM\Column(type="text", nullable=true, options={"default": 0})
    */
   private ?bool $isHighlight;

   public function getId(): ?int
   {
      return $this->id;
   }

   public function setImageFile(File $image = null)
   {
      $this->imageFile = $image;

      // VERY IMPORTANT:
      // It is required that at least one field changes if you are using Doctrine,
      // otherwise the event listeners won't be called and the file is lost
      if ($image) {
         // if 'updatedAt' is not defined in your entity, use another property
         $this->updatedAt = new DateTime('now');
      }
   }

   public function getImageFile(): ?File
   {
      return $this->imageFile;
   }

   public function setImage($image): self
   {
      $this->image = $image;
      return $this;
   }

   /**
    * @return string|null
    */
   public function getImage(): ?string
   {
      return $this->image;
   }

   public function getName(): ?string
   {
      return $this->name;
   }

   public function setName(string $name): self
   {
      $this->name = $name;

      return $this;
   }

   /**
    * @return DateTime
    */
   public function getUpdatedAt(): DateTime
   {
      return $this->updatedAt;
   }

   /**
    * @param DateTime $updatedAt
    * @return Images
    */
   public function setUpdatedAt(DateTime $updatedAt): Images
   {
      $this->updatedAt = $updatedAt;
      return $this;
   }

   public function getSelectFormString(): string
   {
      return $this->getImage();
   }

   public function getProject(): ?Project
   {
      return $this->project;
   }

   public function setProject(?Project $project): self
   {
      $this->project = $project;

      return $this;
   }

   public function getSocial(): ?Social
   {
      return $this->social;
   }

   public function setSocial(Social $social): self
   {
      // set the owning side of the relation if necessary
      if ($social->getImage() !== $this) {
         $social->setImage($this);
      }

      $this->social = $social;

      return $this;
   }

   public function getIsHighlight(): ?bool
   {
      return $this->isHighlight;
   }

   public function setIsHighlight(bool $isHighlight): self
   {
      $this->isHighlight = $isHighlight;

      return $this;
   }
}
