<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @Vich\Uploadable
 */
class Project
{
   /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
   private $id;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Length(
    *    min=3, minMessage="Le nom du projet est trop court, {{ limit }} caractères minimum",
    *    max=80, maxMessage="Le nom du projet est trop long, {{ limit }} caractères maximum"
    * )
    */
   private $name;

   /**
    * @ORM\Column(type="text")
    */
   private $description;

   /**
    * @ORM\Column(type="string", length=255, nullable=true)
    * @Assert\Regex(
    *    pattern="/[a-zA-Z0-9._-]{2,256}\.[a-z]{2,4}\b([a-zA-Z0-9-\/]*)/",
    *    message="Votre url n'est pas valide"
    * )
    */
   private $link;

   /**
    * @ORM\Column(type="datetime")
    * @Assert\NotBlank()
    */
   private $createdAt;

   /**
    * @ORM\OneToMany(targetEntity=Images::class, mappedBy="project")
    */
   private $picture;

   /**
    * @ORM\ManyToMany(targetEntity=Technology::class, inversedBy="projects")
    */
   private $technology;

   /**
    * @ORM\Column(type="string", length=255)
    */
   private $slug;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank()
    */
   private $frame;

   /**
    * @ORM\Column(type="string", length=255)
    * @Assert\NotBlank()
    * @Assert\Length(
    *    max=255,
    *    maxMessage="Vous ne devez pas excéder {{ limit }} caractères"
    * )
    */
   private $short_description;

   public function __construct()
   {
      $this->picture = new ArrayCollection();
      $this->technology = new ArrayCollection();
   }

   public function getId(): ?int
   {
      return $this->id;
   }

   public function getName(): ?string
   {
      return $this->name;
   }

   public function setName(string $name): self
   {
      $this->name = $name;

      return $this;
   }

   public function getDescription(): ?string
   {
      return $this->description;
   }

   public function setDescription(string $description): self
   {
      $this->description = $description;

      return $this;
   }

   public function getLink(): ?string
   {
      return $this->link;
   }

   public function setLink(?string $link): self
   {
      $this->link = $link;

      return $this;
   }

   public function getCreatedAt(): ?DateTimeInterface
   {
      return $this->createdAt;
   }

   public function setCreatedAt(DateTimeInterface $createdAt): self
   {
      $this->createdAt = $createdAt;

      return $this;
   }

   /**
    * @return Collection|Images[]
    */
   public function getPicture(): Collection
   {
      return $this->picture;
   }

   public function addPicture(Images $picture): self
   {
      if (!$this->picture->contains($picture)) {
         $this->picture[] = $picture;
         $picture->setProject($this);
      }

      return $this;
   }

   public function removePicture(Images $picture): self
   {
      if ($this->picture->removeElement($picture)) {
         // set the owning side to null (unless already changed)
         if ($picture->getProject() === $this) {
            $picture->setProject(null);
         }
      }

      return $this;
   }

   /**
    * @return Collection|Technology[]
    */
   public function getTechnology(): Collection
   {
      return $this->technology;
   }

   public function addTechnology(Technology $technology): self
   {
      if (!$this->technology->contains($technology)) {
         $this->technology[] = $technology;
      }

      return $this;
   }

   public function removeTechnology(Technology $technology): self
   {
      $this->technology->removeElement($technology);

      return $this;
   }

   public function getSlug(): ?string
   {
      return $this->slug;
   }

   public function setSlug(string $slug): self
   {
      $this->slug = $slug;

      return $this;
   }

   public function getFrame(): ?string
   {
      return $this->frame;
   }

   public function setFrame(string $frame): self
   {
      $this->frame = $frame;

      return $this;
   }

   public function getShortDescription(): ?string
   {
      return $this->short_description;
   }

   public function setShortDescription(string $short_description): self
   {
      $this->short_description = $short_description;

      return $this;
   }
}
