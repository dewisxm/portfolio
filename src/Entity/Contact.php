<?php


namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
   /**
    * @Assert\NotBlank()
    */
   private string $firstname;

   /**
    * @Assert\NotBlank()
    */
   private string $lastname;

   /**
    * @Assert\NotBlank()
    * @Assert\Email()
    */
   private string $email;

   /**
    * @Assert\NotBlank()
    */
   private string $message;

   /**
    * @return string
    */
   public function getFirstname(): string
   {
      return $this->firstname;
   }

   /**
    * @param mixed $firstname
    * @return Contact
    */
   public function setFirstname(mixed $firstname): static
   {
      $this->firstname = $firstname;
      return $this;
   }

   /**
    * @return string
    */
   public function getLastname(): string
   {
      return $this->lastname;
   }

   /**
    * @param mixed $lastname
    * @return Contact
    */
   public function setLastname(mixed $lastname): static
   {
      $this->lastname = $lastname;
      return $this;
   }

   /**
    * @return string
    */
   public function getEmail(): string
   {
      return $this->email;
   }

   /**
    * @param mixed $email
    * @return Contact
    */
   public function setEmail(mixed $email): static
   {
      $this->email = $email;
      return $this;
   }

   /**
    * @return string
    */
   public function getMessage(): string
   {
      return $this->message;
   }

   /**
    * @param mixed $message
    * @return Contact
    */
   public function setMessage(mixed $message): static
   {
      $this->message = $message;
      return $this;
   }
}
