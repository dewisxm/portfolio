<?php

namespace App\Form;

use App\Entity\Images;
use App\Entity\Information;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InformationType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
         ->add('lastname', TextType::class, [
            'label' => 'Nom',
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('firstname', TextType::class, [
            'label' => 'Prénom',
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('phone', TextType::class, [
            'label' => 'Téléphone',
            'help' => 'Format 0x.xx.xx.xx.xx',
            'required' => false,
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('email', EmailType::class, [
            'label' => 'Email',
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('job', TextType::class, [
            'label' => 'Intutilé du poste',
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('city', TextType::class, [
            'label' => 'Ville',
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('postalCode', TextType::class, [
            'label' => 'Code postal',
            'attr' => ['class' => 'data-user col-9']
         ])
         ->add('image', EntityType::class, [
            'class' => Images::class,
            'choice_label' => 'name',
            'mapped' => true,
            'required' => false
         ])
         ->add('description', TextareaType::class, [
            'label' => 'Description',
            'required' => true,
            'attr' => [
               'class' => 'data-user col-9',
               'rows' => 5
            ]
         ]);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults([
         'data_class' => Information::class,
      ]);
   }
}
