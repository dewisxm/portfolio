<?php

namespace App\Form;

use App\Entity\Images;
use App\Entity\Social;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use function Sodium\add;

class SocialType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
         ->add('name')
         ->add('image', EntityType::class, [
            'class' => Images::class,
            'choice_label' => 'name',
            'placeholder' => 'Sélectionner une image',
            'multiple' => false,
            'expanded' => false,
            'required' => true,
         ])
         ->add('link');
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults([
         'data_class' => Social::class,
      ]);
   }
}
