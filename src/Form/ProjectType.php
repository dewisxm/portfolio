<?php

namespace App\Form;

use App\Entity\Images;
use App\Entity\Project;
use App\Entity\Technology;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
         ->add('name')
         ->add('description', TextareaType::class)
         ->add('link')
         ->add('frame')
         ->add('shortDescription', TextareaType::class, [
            'attr' => ['rows' => 4]
         ])
         ->add('createdAt', DateType::class, [
            'widget' => 'single_text',
         ])
         ->add('picture', EntityType::class, [
            'class' => Images::class,
            'choice_label' => 'name',
            'multiple' => true,
            'by_reference' => false,
            'mapped' => true,
            'required' => false
         ])
         ->add('technology', EntityType::class, [
            'class' => Technology::class,
            'choice_label' => 'name',
            'multiple' => true,
            'by_reference' => false,
            'mapped' => true,
            'required' => false
         ]);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults([
         'data_class' => Project::class,
      ]);
   }
}
