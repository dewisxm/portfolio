<?php

namespace App\Form;

use App\Entity\Categories;
use App\Entity\Images;
use App\Entity\Technology;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TechnologyType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
   {
      $builder
         ->add('name')
         ->add('image', EntityType::class, [
            'class' => Images::class,
            'choice_label' => 'name',
            'placeholder' => 'Sélectionner une image',
            'multiple' => false,
            'expanded' => false,
            'required' => true,
         ])
         ->add('category', EntityType::class, [
            'class' => Categories::class,
            'choice_label' => 'name',
            'placeholder' => 'Sélectionner une catégorie',
            'multiple' => false,
            'expanded' => false,
            'required' => true,
         ]);
   }

   public function configureOptions(OptionsResolver $resolver)
   {
      $resolver->setDefaults([
         'data_class' => Technology::class,
      ]);
   }
}
