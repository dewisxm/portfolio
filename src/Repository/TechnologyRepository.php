<?php

namespace App\Repository;

use App\Entity\Technology;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Technology|null find($id, $lockMode = null, $lockVersion = null)
 * @method Technology|null findOneBy(array $criteria, array $orderBy = null)
 * @method Technology[]    findAll()
 * @method Technology[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechnologyRepository extends ServiceEntityRepository
{
   public function __construct(ManagerRegistry $registry)
   {
      parent::__construct($registry, Technology::class);
   }

   /**
    * @return Technology[] Returns an array of Technology objects
    */
   public function getTechnologies(): array
   {
      return $this->createQueryBuilder('t')
         ->orderBy('t.category', 'ASC')
         ->getQuery()
         ->getResult();
   }

   public function Search(string $term): Query
   {
      return $this->createQueryBuilder('s')
         ->andWhere('s.name LIKE :searchTerm')
         ->setParameter('searchTerm', '%' . $term . '%')
         ->orderBy('s.id', 'DESC')
         ->getQuery();
   }
}
