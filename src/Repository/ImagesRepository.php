<?php

namespace App\Repository;

use App\Entity\Images;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Images|null find($id, $lockMode = null, $lockVersion = null)
 * @method Images|null findOneBy(array $criteria, array $orderBy = null)
 * @method Images[]    findAll()
 * @method Images[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImagesRepository extends ServiceEntityRepository
{
   public function __construct(ManagerRegistry $registry)
   {
      parent::__construct($registry, Images::class);
   }

   public function Search(string $term): Query
   {
      return $this->createQueryBuilder('s')
         ->andWhere('s.name LIKE :searchTerm')
         ->setParameter('searchTerm', '%' . $term . '%')
         ->orderBy('s.id', 'DESC')
         ->getQuery();
   }


   public function isHighlight(int $imageId, string $isHighlight): bool
   {
      /**
       * highlighting of image select
       */
      $this->createQueryBuilder('i')
         ->update()
         ->set('i.isHighlight', $isHighlight)
         ->andWhere('i.id=:id')
         ->setParameter('id', $imageId)
         ->getQuery()
         ->execute();

      return true;
   }
}
