<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\InformationRepository;
use App\Repository\SocialRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
   /**
    * @param InformationRepository $informationRepository
    * @param SocialRepository      $socialRepository
    * @param Request               $request
    * @param MailerInterface       $mailer
    * @return Response
    * @throws TransportExceptionInterface
    */
   #[Route('/contact', name: 'contact')]
   public function index(
      InformationRepository $informationRepository,
      SocialRepository $socialRepository,
      Request $request,
      MailerInterface $mailer
   ): Response
   {

      $contact = new Contact();
      $form = $this->createForm(ContactType::class, $contact);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $email = (new Email())
            ->from('contact@dierick-dewi.fr')
            ->to('dierickd@outlook.fr')
            ->subject('Nouvelle demande de contact !')
            ->html($this->renderView('emails/notifications.html.twig', [
               'contact' => $contact,
            ]), 'utf-8');

         $mailer->send($email);
         $this->addFlash('success', 'Message envoyé avec succès');

         return $this->redirectToRoute('home');
      }
      return $this->render('pages/contact.html.twig', [
         'information' => $informationRepository->findAll()[0],
         'socials' => $socialRepository->findAll(),
         'form' => $form->createView(),
      ]);
   }

}
