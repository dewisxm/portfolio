<?php


namespace App\Controller;

use App\Repository\InformationRepository;
use App\Repository\ProjectRepository;
use App\Repository\SocialRepository;
use App\Services\ProjectService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/project')]
class ProjectPublicController extends AbstractController
{

   #[Route('/detail/{slug}', name: 'project_detail', methods: ['GET'])]
   public function detailProject(
      Request $request,
      ProjectService $projectService,
      InformationRepository $informationRepository,
      SocialRepository $socialRepository
   ): Response
   {
      return $this->render('pages/projectDetail.html.twig', [
         'project' => $projectService->getProjectBySlug($request)[0],
         'socials' => $socialRepository->findAll(),
         'information' => $informationRepository->findAll()[0]
      ]);
   }

   #[Route('/list', name: 'project_list')]
   public function list(
      ProjectRepository $projectRepository,
      InformationRepository $informationRepository,
      SocialRepository $socialRepository
   ): Response
   {
      return $this->render('pages/projectList.html.twig', [
         'projects' => $projectRepository->findAll(),
         'socials' => $socialRepository->findAll(),
         'information' => $informationRepository->findAll()[0]
      ]);
   }
}
