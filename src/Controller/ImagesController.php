<?php

namespace App\Controller;

use App\Entity\Images;
use App\Form\ImagesType;
use App\Form\SearchType;
use App\Repository\ImagesRepository;
use App\Repository\TechnologyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/images')]
class ImagesController extends AbstractController
{
   #[Route('/', name: 'images_index', methods: ['GET', 'POST'])]
   public function index(
      ImagesRepository $imagesRepository,
      PaginatorInterface $paginator,
      Request $request
   ): Response
   {
      $isSearch = false;
      $form = $this->createForm(SearchType::class);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $data = $form->getData();
         $imagesList = $imagesRepository->search(mb_strtolower($data['searchField']));
         $isSearch = true;
      } else {
         $imagesList = $imagesRepository->findBy([], ['name' => 'ASC']);
      }
      return $this->render('admin/images/index.html.twig', [
         'imagesList' => $paginator->paginate(
            $imagesList,
            $request->query->getInt('page', 1),
            PER_PAGE,
         ),
         'form' => $form->createView(),
         'search' => $isSearch,
      ]);
   }

   #[Route('/new', name: 'images_new', methods: ['GET', 'POST'])]
   public function new(Request $request): Response
   {
      $image = new Images();
      $form = $this->createForm(ImagesType::class, $image);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $entityManager = $this->getDoctrine()->getManager();
         $image->setIsHighlight(false);
         $entityManager->persist($image);
         $entityManager->flush();

         $this->addFlash('success', "Sauvegardé avec succès");
         return $this->redirectToRoute('images_index');
      }

      return $this->render('admin/images/new.html.twig', [
         'image' => $image,
         'form' => $form->createView(),
      ]);
   }

   #[Route('/{id}', name: 'images_show', methods: ['GET'])]
   public function show(Images $image): Response
   {
      return $this->render('admin/images/show.html.twig', [
         'image' => $image,
      ]);
   }

   #[Route('/{id}/edit', name: 'images_edit', methods: ['GET', 'POST'])]
   public function edit(Request $request, Images $image): Response
   {
      $form = $this->createForm(ImagesType::class, $image);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $this->getDoctrine()->getManager()->flush();

         $this->addFlash('success', "Sauvegardé avec succès");
         return $this->redirectToRoute('images_index');
      }

      return $this->render('admin/images/edit.html.twig', [
         'image' => $image,
         'form' => $form->createView(),
      ]);
   }

   #[Route('/{id}', name: 'images_delete', methods: ['DELETE'])]
   public function delete(
      Request $request,
      Images $image,
      TechnologyRepository $technologyRepository
   ): Response
   {
      if ($this->isCsrfTokenValid('delete' . $image->getId(), $request->request->get('_token'))) {
         $techno = $technologyRepository->findBy(['image' => $image->getId()]);
         if ($techno) {
            $this->addFlash('danger',
               "L'image '" . $techno[0]->getImage()->getName() . "' est actuellement en cours d'utilisation!
            Supprimer la relation avant de supprimer l'image.");
         } else {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($image);
            $entityManager->flush();
            $this->addFlash('success', "Image supprimée avec succès");
         }
      }

      return $this->redirectToRoute('images_index');
   }
}
