<?php

namespace App\Controller;

use App\Repository\InformationRepository;
use App\Repository\SocialRepository;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
   /**
    * @Route("/login", name="app_login")
    * @param AuthenticationUtils   $authenticationUtils
    * @param InformationRepository $information
    * @param SocialRepository      $socialRepository
    * @return Response
    */
   public function login(
      AuthenticationUtils $authenticationUtils,
      InformationRepository $information,
      SocialRepository $socialRepository
   ): Response
   {
      if ($this->getUser()) {
         return $this->redirectToRoute('admin');
      }

      // get the login error if there is one
      $error = $authenticationUtils->getLastAuthenticationError();
      // last username entered by the user
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('security/login.html.twig', [
         'last_username' => $lastUsername,
         'error' => $error,
         'information' => $information->findAll()[0],
         'socials' => $socialRepository->findAll(),
      ]);
   }

   /**
    * @Route("/logout", name="app_logout")
    */
   public function logout()
   {
      throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
   }
}
