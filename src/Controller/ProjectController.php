<?php

namespace App\Controller;

use App\Entity\Project;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Services\ImageService;
use App\Services\Slugify;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/project')]
class ProjectController extends AbstractController
{
   #[Route('/', name: 'project_index', methods: ['GET'])]
   public function index(ProjectRepository $projectRepository): Response
   {
      return $this->render('admin/project/index.html.twig', [
         'projects' => $projectRepository->findAll(),
      ]);
   }

   #[Route('/new', name: 'project_new', methods: ['GET', 'POST'])]
   public function new(Request $request, Slugify $slugify): Response
   {
      $project = new Project();
      $form = $this->createForm(ProjectType::class, $project);
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
         $entityManager = $this->getDoctrine()->getManager();
         $project->setSlug($slugify->generate($project->getName()));
         $entityManager->persist($project);
         $entityManager->flush();

         $this->addFlash('success', "Sauvegardé avec succès");
         return $this->redirectToRoute('project_index');
      }

      return $this->render('admin/project/new.html.twig', [
         'project' => $project,
         'form' => $form->createView(),
      ]);
   }

   #[Route('/{id}', name: 'project_show', methods: ['GET'])]
   public function show(Project $project): Response
   {
      return $this->render('admin/project/show.html.twig', [
         'project' => $project,
      ]);
   }

   #[Route('/{id}/edit', name: 'project_edit', methods: ['GET', 'POST'])]
   public function edit(Request $request, Project $project, Slugify $slugify): Response
   {
      $form = $this->createForm(ProjectType::class, $project);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $project->setSlug($slugify->generate($project->getName()));
         $this->getDoctrine()->getManager()->flush();

         $this->addFlash('success', "Sauvegardé avec succès");
         return $this->redirectToRoute('project_show', ['id' => $request->get('id')]);
      }

      return $this->render('admin/project/edit.html.twig', [
         'project' => $project,
         'form' => $form->createView(),
      ]);
   }

   #[Route('/{id}', name: 'project_delete', methods: ['DELETE'])]
   public function delete(Request $request, Project $project): Response
   {
      if ($this->isCsrfTokenValid('delete' . $project->getId(), $request->request->get('_token'))) {
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->remove($project);
         $entityManager->flush();
      }

      return $this->redirectToRoute('project_index');
   }

   /**
    * @param Request      $request
    * @param ImageService $imageService
    * @return RedirectResponse
    */
   #[Route('/{id}/highlight/{image}', name: 'project_highlight')]
   public function highlight(Request $request, ImageService $imageService): RedirectResponse
   {
      $imageService->highlight($request);
      return $this->redirectToRoute('project_show', ['id' => $request->get('id')]);
   }
}

