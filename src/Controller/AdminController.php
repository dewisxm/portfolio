<?php

namespace App\Controller;

use App\Services\countVisitedService;
use JetBrains\PhpStorm\NoReturn;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
   /**
    * @IsGranted("ROLE_ADMIN")
    * @param countVisitedService $countVisitedService
    * @return Response
    */

   #[Route('/admin', name: 'admin')]
   public function index(countVisitedService $countVisitedService): Response
   {
      return $this->render('admin/index.html.twig', [
         'counter' => $countVisitedService->numberVisited(),
      ]);
   }
}
