<?php

namespace App\Controller;

use App\Repository\CategoriesRepository;
use App\Repository\InformationRepository;
use App\Repository\ProjectRepository;
use App\Repository\SocialRepository;
use App\Repository\TechnologyRepository;
use App\Services\countVisitedService;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
   #[NoReturn]
   public function __construct(countVisitedService $countVisitedService)
   {
      $countVisitedService->countVisited();
   }

   #[Route('/', name: 'home')]
   public function index(
      InformationRepository $information,
      ProjectRepository $project,
      TechnologyRepository $technology,
      CategoriesRepository $categories,
      SocialRepository $socialRepository
   ): Response
   {
      return $this->render('pages/index.html.twig', [
         'information' => $information->findAll()[0],
         'projects' => $project->findBy([], ['id' => 'DESC'], 3),
         'technologies' => $technology->getTechnologies(),
         'categories' => $categories->findAll(),
         'socials' => $socialRepository->findAll(),
      ]);
   }
}
