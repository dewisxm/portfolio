<?php

namespace App\Controller;

use App\Entity\Technology;
use App\Form\SearchType;
use App\Form\TechnologyType;
use App\Repository\TechnologyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use const PER_PAGE;

#[Route('/admin/technology')]
class TechnologyController extends AbstractController
{
   #[Route('/', name: 'technology_index', methods: ['GET', 'POST'])]
   public function index(
      TechnologyRepository $technologyRepository,
      PaginatorInterface $paginator,
      Request $request
   ): Response
   {

      $isSearch = false;
      $form = $this->createForm(SearchType::class);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $data = $form->getData();
         $technologies = $technologyRepository->search(mb_strtolower($data['searchField']));
         $isSearch = true;
      } else {
         $technologies = $technologyRepository->findBy([], ['name' => 'ASC']);
      }

      return $this->render('admin/technology/index.html.twig', [
         'technologies' => $paginator->paginate(
            $technologies,
            $request->query->getInt('page', 1),
            PER_PAGE
         ),
         'form' => $form->createView(),
         'search' => $isSearch,
      ]);
   }

   #[Route('/new', name: 'technology_new', methods: ['GET', 'POST'])]
   public function new(Request $request): Response
   {
      $technology = new Technology();
      $form = $this->createForm(TechnologyType::class, $technology);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->persist($technology);
         $entityManager->flush();

         $this->addFlash('success', "Sauvegardé avec succès");
         return $this->redirectToRoute('technology_index');
      }

      return $this->render('admin/technology/new.html.twig', [
         'technology' => $technology,
         'form' => $form->createView(),
      ]);
   }

   #[Route('/{id}', name: 'technology_show', methods: ['GET'])]
   public function show(Technology $technology): Response
   {
      return $this->render('admin/technology/show.html.twig', [
         'technology' => $technology,
      ]);
   }

   #[Route('/{id}/edit', name: 'technology_edit', methods: ['GET', 'POST'])]
   public function edit(Request $request, Technology $technology): Response
   {
      $form = $this->createForm(TechnologyType::class, $technology);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $this->getDoctrine()->getManager()->flush();

         $this->addFlash('success', "Sauvegardé avec succès");
         return $this->redirectToRoute('technology_index');
      }

      return $this->render('admin/technology/edit.html.twig', [
         'technology' => $technology,
         'form' => $form->createView(),
      ]);
   }

   #[Route('/{id}', name: 'technology_delete', methods: ['DELETE'])]
   public function delete(Request $request, Technology $technology): Response
   {
      if ($this->isCsrfTokenValid('delete' . $technology->getId(), $request->request->get('_token'))) {
         $entityManager = $this->getDoctrine()->getManager();
         $entityManager->remove($technology);
         $entityManager->flush();
      }

      return $this->redirectToRoute('technology_index');
   }
}
