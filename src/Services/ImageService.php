<?php


namespace App\Services;


use App\Repository\ImagesRepository;
use Symfony\Component\BrowserKit\Request;

class ImageService
{
   private ImagesRepository $imagesRepository;

   public function __construct(ImagesRepository $imagesRepository)
   {
      $this->imagesRepository = $imagesRepository;
   }

   /**
    * 1 when is highlight
    * 0 when is not highlight
    * @param Request $request
    * @return bool
    */
   public function highlight(Request $request): bool
   {
      $projectId = $request->get('id');
      $imageId = $request->get('image');

      $hasHighlight = $oldIsHighlight = $this->imagesRepository->findBy(
         [
            "project" => $projectId,
            "isHighlight" => true
         ]
      );
      if ($hasHighlight) {
         $this->imagesRepository->isHighlight($oldIsHighlight[0]->getId(), '0');
      }
      $this->imagesRepository->isHighlight($imageId, '1');
      return true;
   }

}
