<?php


namespace App\Services;


use App\Repository\ProjectRepository;
use Symfony\Component\HttpFoundation\Request;

class ProjectService
{

   private ProjectRepository $projectRepository;

   public function __construct(ProjectRepository $projectRepository)
   {
      $this->projectRepository = $projectRepository;
   }

   public function getProjectBySlug(Request $request): array
   {
      $slug = $request->get('slug');
      return $this->projectRepository->findBy(['slug' => $slug]);
   }

}
