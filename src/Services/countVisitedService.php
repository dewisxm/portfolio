<?php

namespace App\Services;

use JetBrains\PhpStorm\NoReturn;

class countVisitedService
{
   private string $generalCount;
   private string $dayCount;
   private int $guest_number;

   public function __construct()
   {
      $this->generalCount = dirname(dirname(__DIR__)) . '/data/visited';
      if (!file_exists($this->generalCount)) {
         file_put_contents($this->generalCount, "1");
      }
      $this->guest_number = (int)file_get_contents($this->generalCount);
   }

   #[NoReturn]
   public function countVisited(): void
   {
      $this->guest_number++;
      file_put_contents($this->generalCount, $this->guest_number);
   }

   public function numberVisited(): int
   {
      return (int)file_get_contents($this->generalCount);
   }
}
