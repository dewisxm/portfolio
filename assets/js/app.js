/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// start the Stimulus application
import '../app.scss';

const $ = require('jquery');
require('bootstrap');
require('select2');

$(document).ready(() => {
    let toastShow = document.querySelector('.toast');
    if (toastShow) {
        setTimeout(() => {
            toastShow.classList.add('hide');
        }, 3500);
    }
    
    $('select').select2();
    tinymce.init({
        selector: 'textarea.tiny',
        menubar: false,
        toolbar_sticky: true,
        toolbar: 'undo redo styleselect bold italic alignleft aligncenter alignright bullist numlist outdent indent code',
        plugins: 'code'
    });
    //
    // let toastElList = [].slice.call(document.querySelectorAll('.toast'))
    // toastElList.map(function (toastEl) {
    //     return new bootstrap.Toast(toastEl, toastEl.show)
    // })
});

$('#modal1').on('shown.bs.modal', function () {
    $('#btn1').trigger('focus')
})

$('.target-menu').on('click', e => {
    e.preventDefault();
    $('.navbar-mobile-bottom').toggleClass('open-menu');
})


window.addEventListener('scroll', (e) => {
    e.preventDefault();
    
    let navPublic = document.getElementById('nav-public');
    let navBrand = document.getElementById('brand');
    if (navPublic) {
        if (window.scrollY <= 100) {
            navPublic.style.height = '100px';
            navPublic.style.boxShadow = 'none';
            navBrand.style.fontSize = "24px";
            navPublic.style.backgroundColor = 'transparent';
        } else {
            navPublic.style.height = '56px';
            navPublic.style.boxShadow = '0 1px 10px 0px rgba(206, 206, 206, 0.75)';
            navBrand.style.fontSize = "20px";
            navPublic.style.backgroundColor = '#fdfbf4';
        }
    }
});

